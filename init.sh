#!/bin/sh

psql -U "$POSTGRES_USER" "$POSTGRES_DB" <<EOF
DROP TABLE IF EXISTS commande;
CREATE TABLE commande(com TEXT, def TEXT);
INSERT INTO commande (com, def) VALUES ('com1','def1'),('com2','def2');
EOF
