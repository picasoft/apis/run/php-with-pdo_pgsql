<?php
    echo "<h1>Dix commandes très utiles sur Ubuntu</h1></br>";
    echo "<ol>";
    $connexion = new PDO('pgsql:host=db;port=5432;dbname=api', 'api', 'api');
    $sql = 'SELECT * FROM commande';
    $results = $connexion->prepare($sql);
    $results->execute();
    while ($row = $results->fetch(PDO::FETCH_ASSOC)){
        echo "<li><b>" . $row['com'] . "</b> : ";
        echo $row['def'] . "</li>";
    }
    echo "</ol>";
?>
