FROM php:8.2-fpm-alpine

RUN apk add --no-cache postgresql-libs postgresql-dev \
    && docker-php-ext-install pcntl pdo_pgsql \
    && apk del postgresql-dev

