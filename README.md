<!-- LTeX: language=fr -->
# Image Docker php-with-pdo_pgsql

Cette image se base sur l'image PHP Docker [officielle](https://hub.docker.com/_/php).
Elle ajoute seulement les extensions PHP nécessaires pour se connecter à PostgreSQL.

## Construire une image plus récente

Il est de bon ton de reconstruire l'image avant l'API pour la mettre à jour :
```bash
docker build -t registry.gitlab.utc.fr/picasoft/apis/run/php-with-pdo_pgsql .
docker push registry.gitlab.utc.fr/picasoft/apis/run/php-with-pdo_pgsql
```

On peut vérifier s'il existe un tag PHP plus récent [upstream](https://hub.docker.com/_/php).

Pour vérifier que la connexion PHP → PostgreSQL fonctionne, on peut lancer un `docker compose up` et vérifier http://localhost:80

On devrait y trouver le texte suivant :
```markdown
# Dix commandes très utiles sur Ubuntu

1. com1: def1
2. com2: def2
```
